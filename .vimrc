set number
set ts=4 sw=4
set expandtab
set cc=80
set fillchars=fold:\ 
inoremap kj <esc>
au BufNewFile,BufRead *.js,*.html,*.css
    \ setlocal tabstop=2 softtabstop=2 shiftwidth=2
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'morhetz/gruvbox'
Plug 'altercation/vim-colors-solarized'
Plug 'lervag/vimtex'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'octol/vim-cpp-enhanced-highlight'
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
let g:vimtex_fold_enabled=1
set conceallevel=1
let g:tex_conceal='abdmg'
hi Conceal ctermbg=NONE
hi Conceal ctermfg=NONE
hi Conceal guifg=NONE
hi Conceal guibg=NONE
Plug 'sirver/ultisnips'
let g:UltiSnipsExpandTrigger = '<tab>'
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'
let g:UltiSnipsEditSplit="vertical"
Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-syntastic/syntastic'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
Plug 'Yggdroot/indentLine'
let g:indentLine_faster=1
let g:indentLine_setColors=0
call plug#end()
colorscheme gruvbox

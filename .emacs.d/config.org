#+TITLE: Pab's Emacs Config
#+AUTHOR: Pablo Alcalde
#+STARTUP: overview
* PACKAGE MANAGEMENT
** Setup package.el to work with MELPA.
#+begin_src emacs-lisp
  (require 'package)
  (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/") t)
  (add-to-list 'package-archives
	       '("org" . "https://orgmode.org/elpa/") t)
  (add-to-list 'package-archives
	       '("elpa" . "https://elpa.gnu.org/packages/") t)
  (package-refresh-contents)
  (package-initialize)
#+end_src

** Installing use-package
#+begin_src emacs-lisp
  (unless (package-installed-p 'use-package)
    (package-install 'use-package))
#+end_src
* PERSONAL INFORMATION
#+begin_src emacs-lisp
  (setq user-full-name "Pablo C. Alcalde"
      user-mail-address "paalcald@ucm.es")
#+end_src
* BACKUP FILES
  #+begin_src emacs-lisp
    (setq backup-directory-alist
	  `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
	  `((".*" ,temporary-file-directory t)))
    (message "Deleting old backup files...")
    (let ((week (* 60 60 24 7))
	  (current (float-time (current-time))))
      (dolist (file (directory-files temporary-file-directory t))
	(when (and (backup-file-name-p file)
		   (> (- current (float-time (nth 4 (file-attributes file))))
		      week))
	  (message "%s" file)
	  (delete-file file))))
  #+end_src
* FONTS
** Fira Code font
#+begin_src emacs-lisp
    (set-face-attribute 'default nil
			:family "Fira Code"
			:weight 'normal
			:height 130)
    (add-to-list 'default-frame-alist '(family "Fira Code"))
    ;;(set-fontset-font (frame-parameter nil 'font)
    ;;  'japanese-jisx0208
  ;;  '("IPAGOTHIC" . "unicode-bmp"))
#+end_src
** Ligature
  #+begin_src emacs-lisp
    (use-package ligature
      :load-path "~/.emacs.d/ligature.el"
      :config
      ;; Enable the "www" ligature in every possible major mode
      (ligature-set-ligatures 't '("www"))
      ;; Enable traditional ligature support in eww-mode, if the
      ;; `variable-pitch' face supports it
      (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
      ;; Enable all Cascadia Code ligatures in programming modes
      (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
					   ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
					   "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
					   "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
					   "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
					   "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
					   "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
					   "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!"
					   ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|"
					   "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
					   "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
					   "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
					   "\\\\" "://"))
      ;; Enables ligature checks globally in all buffers. You can also do it
      ;; per mode with `ligature-mode'.
      (global-ligature-mode t))
#+end_src
* INPUT METHODS
  +begin_src emacs-lisp
	(use-package mozc
	  :ensure t
	  :config
	  (setq default-input-method "japanese-mozc")
	  )
	  +end_src
* THEME
#+begin_src emacs-lisp
  ;; (use-package doom-themes
  ;;   :ensure t
  ;;   :config
  ;;   (setq doom-themes-enable-bold t
  ;; 	doom-themes-enable-italic t)
  ;;     (load-theme 'gruvbox-light-medium t)
  ;;     (doom-themes-org-config))
  (use-package kaolin-themes
   :ensure t
   :config
   (load-theme 'gruvbox-light-medium t))
#+end_src
* ORG Mode
** Basics
   #+begin_src emacs-lisp
     (require 'org)
     (define-key global-map "\C-cl" 'org-store-link)
     (define-key global-map "\C-ca" 'org-agenda)
     (define-key global-map "\C-cc" 'org-capture)
     (setq org-log-done 'time)
     (setq org-ellipsis " ▾") ;;▼
     (add-to-list 'org-structure-template-alist '("n" . "notes"))
     (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.6))
   #+end_src
** Agenda
   #+begin_src emacs-lisp
	  (setq org-agenda-files (list "~/Dropbox/OrgFiles/tasks.org"))
	  (setq org-agenda-start-with-log-mode t)
	  (setq org-log-done 'time)
	  (setq org-done-into-drawer t)
	  (setq org-deadline-warning-days 10)
	  (setq org-refile-targets '(("archive.org" :maxlevel . 1)))
	  (setq org-capture-templates
		`(("t" "Tasks / Projects")
		  ("tp" "Personal Task" entry (file+olp "~/Dropbox/OrgFiles/tasks.org" "Personal")
		   "* TODO %? :personal:\n  %U\n  %a\n  %i" :empty-lines 1)
		  ("tu" "University Task" entry (file+olp "~/Dropbox/OrgFiles/tasks.org" "University")
		   "* TODO %? :university:\n  %U\n  %a\n  %i" :empty-lines 1)
		  ("tb" "Bar Task" entry (file+olp "~/Dropbox/OrgFiles/tasks.org" "Bar")
		   "* TODO %? :bar:\n  %U\n  %a\n  %i" :empty-lines 1)
		  ("tp" "Quiro Task" entry (file+olp "~/Dropbox/OrgFiles/tasks.org" "Quiropractice")
		   "* TODO %? :quiropractice:\n  %U\n  %a\n  %i" :empty-lines 1)))
   #+end_src
** Get Pretty Bullets
#+begin_src emacs-lisp
  (use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
#+end_src
<<<<<<< HEAD
** Babel Languages
   #+begin_src emacs-lisp
	   (org-babel-do-load-languages
	    'org-babel-load-languages '((C . t)
					(python . t)
					(haskell . t)))
   #+end_src
** Export markdown
   #+begin_src emacs-lisp
     (eval-after-load "org"
       '(require 'ox-md nil t))
   #+end_src
** PDF Tools
   #+begin_src emacs-lisp
     (use-package pdf-tools
       :ensure t
       :config
       (pdf-tools-install))
   #+end_src
* REVEAL.JS
  #+begin_src emacs-lisp
    (use-package ox-reveal
      :after org
      :ensure t
      :config
      (setq org-reveal-root "file:///home/pab/reveal.js")
      (add-to-list 'org-structure-template-alist '("n" . "notes")))
  #+end_src
* ROAM
  #+begin_src emacs-lisp
    (use-package org-roam
      :ensure t
      :init
      (setq org-roam-v2-ack t)
      :custom
      (org-roam-directory "~/Dropbox/OrgRoam")
      (org-roam-completion-everywhere t)
      (org-roam-capture-templates
       '(("d" "default" plain
	  "%?"
	  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
	  :unnarrowed t)
	 ("l" "data structure" plain
	  "* Characteristics \n%?\n * Uses \n\n* Implementations"
	  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
	  :unnarrowed t)))
      :bind (("C-c n l" . org-roam-buffer-toggle)
	     ("C-c n f" . org-roam-node-find)
	     ("C-c n i" . org-roam-node-insert)
	     ("C-c DEL" . org-mark-ring-goto)
	     :map org-mode-map
	     ("C-M-i" . completion-at-point))
      :config
      (org-roam-setup))
  #+end_src
* GRAPHICAL USER INTERFACE SETTINGS
** Dashboard:
#+begin_src emacs-lisp
    (use-package dashboard
      :ensure t
      :config
      (dashboard-setup-startup-hook)
    (setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
    ;; Set the banner
    (setq dashboard-startup-banner "/home/pab/.emacs.d/logo/orange-logo.png")
    ;; Value can be
    ;; 'official which displays the official emacs logo
    ;; 'logo which displays an alternative emacs logo
    ;; 1, 2 or 3 which displays one of the text banners
    ;; "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever image/text you would prefer

    ;; Content is not centered by default. To center, set
    ;;(setq dashboard-center-content t)

    ;; To disable shortcut "jump" indicators for each section, set
    ;;(setq dashboard-show-shortcuts nil)
    (setq dashboard-items '((recents . 5)
			    (bookmarks . 5)
			    (agenda . 2)
			    (registers . 5)))
    ;; Removing org agenda files from recent files list
    (add-to-list 'recentf-exclude (org-agenda-files))
    ;; Removing elpa files from recent files list
    (add-to-list 'recentf-exclude (format "%s/\\.emacs\\.d/elpa/.*" (getenv "HOME")))
    (setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*"))))
  (dashboard-refresh-buffer)
#+end_src
** Modeline
   #+begin_src emacs-lisp
     (use-package doom-modeline
       :ensure t
       :config 
       (doom-modeline-mode 1))
#+end_src
** Menubar, Toolbar, Scrollbar
   #+begin_src emacs-lisp
     (menu-bar-mode -1)
     (tool-bar-mode -1)
     (scroll-bar-mode -1)
   #+end_src
** Line Numbers
   Toggle line numbers only in prog-mode and in text-mode.
   #+begin_src emacs-lisp
     (add-hook 'prog-mode-hook (lambda () (display-line-numbers-mode 1)))
   #+end_src
** Mouse Scrolling Slowed Down
#+begin_src emacs-lisp
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
#+end_src
** Icons
   #+begin_src emacs-lisp
     (use-package all-the-icons)
   #+end_src
** Recent files cleanup
  #+begin_src emacs-lisp

  #+end_src
* MAGIT
  #+begin_src emacs-lisp
    (use-package magit
      :ensure t)
  #+end_src
* PROOF GENERAL
  #+begin_src emacs-lisp
    (use-package proof-general
      :ensure t)
  #+end_src
* HASKELL
** Haskell Interactive Mode
   #+begin_src emacs-lisp
     (require 'haskell-interactive-mode)
     (require 'haskell-process)
     (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
     (custom-set-variables
       '(haskell-process-suggest-remove-import-lines t)
       '(haskell-process-auto-import-loaded-modules t)
       '(haskell-process-log t))
     (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
     (define-key haskell-mode-map (kbd "C-`") 'haskell-interactive-bring)
     (define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
     (define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)
     (define-key haskell-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
     (define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
     (define-key haskell-mode-map (kbd "C-c c") 'haskell-process-cabal)
   #+end_src
* BUFFERS
** ibuffer
   #+begin_src emacs-lisp
     (global-set-key (kbd "C-x C-b") 'ibuffer)
   #+end_src
* WINDOWS
** Ace Window
 ;;  #+begin_src emacs-lisp
     (use-package ace-window
     :ensure t
     :init
     (progn
       (global-set-key [remap other-window] 'ace-window)
       (custom-set-faces
	'(aw-leading-char-face
	  ((t (:foreground "#d65d0e" :height 3.0)))))
       ))
   ;; #+end_src
* AUTOCOMPLETE
** interactive completion
   #+begin_src emacs-lisp
     (use-package vertico
       :ensure t
       :init
       (vertico-mode)
       :config
       (setq vertico-cycle t))
   #+end_src
** WHICH KEY
   This package gives suggestions when typping keybindings
   #+begin_src emacs-lisp
     (use-package which-key
       :ensure t
       :config
       (setq which-key-idle-delay 0.3)
       (which-key-mode 1))
   #+end_src
** Comp-any mode
   #+begin_src emacs-lisp
	  (use-package company
	    :ensure t
	    :init
	    :hook ((prog-mode . company-mode)
		   (cmake-mode . company-mode))
	    :config
	    ;;(setq company-backends (delete 'company-semantic company-backends))
	    (add-to-list 'company-backends 'company-c-headers))
   #+end_src
** SaveHist
   #+begin_src emacs-lisp
     (use-package savehist
       :ensure t
       :init
       (savehist-mode))
   #+end_src
** Marginalia
   #+begin_src emacs-lisp
     (use-package marginalia
       :after vertico
       :ensure t
       ;;:custom
       ;;(marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
       :init
       (marginalia-mode))
   #+end_src
* FLYCHECK
  #+begin_src emacs-lisp
    (use-package flycheck
      :ensure t
      :init (global-flycheck-mode)
      (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))
  #+end_src
* YASNIPPET
  #+begin_src emacs-lisp
	(use-package yasnippet
	  :ensure t
	  :init
	  (yas-global-mode 1))
  #+end_src
* KEYBINDS
* MU4E
  #+begin_src emacs-lisp
	(use-package mu4e
	  :ensure nil
	  ;; :load-path "/usr/share/emacs/site-lisp/mu4e/"
	  :config
	  (require 'mu4e-org)
	  ;; Dont ask for concepts, take first instead
	  (setq mu4e-context-policy 'pick-first)
	  ;; This is set to 't' to avoid mail syncing issues when using mbsync
	  (setq mu4e-change-filenames-when-moving t)
	  ;; Refresh mail using isync every 10 minutes
	  (setq mu4e-update-interval (* 10 60))
	  ;; Set mbsync directory and command
	  (setq mu4e-get-mail-command "mbsync -a")
	  (setq mu4e-maildir "~/Mail")
	  ;; Set smtpmail as email sender
	  (require 'smtpmail) 
	  (setq message-send-mail-function 'smtpmail-send-it)
	  ;; Use password-store for authentification
	  (require 'auth-source-pass)
	  (auth-source-pass-enable)
	  (setq auth-sources '(password-store))
	  ;; Set context for diferent email accounts
	  (setq mu4e-compose-context-policy 'ask-if-none)
	  ;;uncomment to turn off automatic col max length
	  ;;(setq mu4e-compose-format-flowed t)
	  (setq mu4e-contexts
		(list
		 (make-mu4e-context
		  :name "UCM"
		  :match-func
		  (lambda (msg)
		    (when msg
		      (string-prefix-p "/UCM" (mu4e-message-field msg :maildir))))
		  :vars '((user-mail-address  .  "paalcald@ucm.es")
			  (user-full-name  .  "Pablo C. Alcalde")
			  (smtpmail-smtp-server  .   "smtp.gmail.com")
			  (smtpmail-smtp-user  . "paalcald@ucm.es")
			  (smtpmail-smtp-service  . 465)
			  (smtpmail-stream-type  . ssl)
			  (mu4e-drafts-folder  .  "/UCM/[Gmail]/Drafts")
			  (mu4e-sent-folder  .  "/UCM/[Gmail]/Sent Mail")
			  (mu4e-refile-folder  .  "/UCM/[Gmail]/All Mail")
			  (mu4e-trash-folder  .  "/UCM/[Gmail]/Trash")))
		 (make-mu4e-context
	       :name "Personal"
	       :match-func
		 (lambda (msg)
		   (when msg
		     (string-prefix-p "/Gmail" (mu4e-message-field msg :maildir))))
	       :vars '((user-mail-address  .  "alcaldepablo.c@gmail.com")
		       (user-full-name  .  "Pablo C. Alcalde")
		       (smtpmail-local-domain  .  nil)
		       (smtpmail-smtp-server  .   "smtp.gmail.com")
		       (smtpmail-smtp-user  . "alcaldepablo.c@gmail.com")
		       (smtpmail-smtp-service  . 465)
		       (smtpmail-stream-type  . ssl)
		       (mu4e-drafts-folder  .  "/Gmail/[Gmail]/Drafts")
		       (mu4e-sent-folder  .  "/Gmail/[Gmail]/Sent Mail")
		       (mu4e-refile-folder  .  "/Gmail/[Gmail]/All Mail")
		       (mu4e-trash-folder  .  "/Gmail/[Gmail]/Trash")))))
	  (setq mu4e-maildir-shortcuts
		'((:maildir "/Gmail/Inbox"    :key ?p)
		  (:maildir "/UCM/Inbox" :key ?u)
		  (:maildir "/Gmail/[Gmail]/Sent Mail" :key ?s)
		  (:maildir "/Gmail/[Gmail]/Trash"     :key ?t)
		  (:maildir "/Gmail/[Gmail]/Drafts"    :key ?d)
		  (:maildir "/Gmail/[Gmail]/All Mail"  :key ?a)))
	  (mu4e t))

  #+end_src
  
